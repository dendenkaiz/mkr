<?php

require_once 'actor.php';
class actors
{
    private $allactors;

    public function __constructor() {
        $actors = array(
            new Actor('michael', 5,1),
            new Actor('john', 1,0),
            new Actor('terry', 8,3),);
        $this->allactors =$actors;
    }
    public function minimumTwoFilms()
    {
        $OverTwoFilms = [];
        foreach ($this->allactors as $actor) {
            if ($actor->getFilmamount()>=2)
            {
                $OverTwoFilms[] = $actor;
            }
        }
        return count($OverTwoFilms);
    }

    public function countOscarActors()
    {
        $OverOneOscar = [];
        foreach ($this->actors as $actor) {
            if ($actor->getOscars())
            {
                $OverOneOscar[] = $actor;
            }
        }
        return count($OverOneOscar);
    }


}