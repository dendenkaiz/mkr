<?php


class actor
{
private $name;
private $filmamount;
private $oscars;

    public function __construct($name, $filmamount, $oscars)
    {
        $this->name = $name;
        $this->filmamount = $filmamount;
        $this->oscars = $oscars;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFilmamount()
    {
        return $this->filmamount;
    }

    /**
     * @param mixed $filmamount
     */
    public function setFilmamount($filmamount)
    {
        $this->filmamount = $filmamount;
    }

    /**
     * @return mixed
     */
    public function getOscars()
    {
        return $this->oscars;
    }

    /**
     * @param mixed $oscars
     */
    public function setOscars($oscars)
    {
        $this->oscars = $oscars;
    }


}